       IDENTIFICATION DIVISION.
       PROGRAM-ID.  ODSAISD1.

      ******************************************************************
      * Creates extract of AI records for RS Data Strategy into RS_ODS
      *
      * #         DATE    PROG  DESCRIPTION
      * W13771  11/28/17  WCT   PROJ13771 RST data integration
      ******************************************************************

       ENVIRONMENT DIVISION.

       INPUT-OUTPUT SECTION.
       FILE-CONTROL.

       SELECT EXTRACT-FILE    ASSIGN TO ODSAI
                              ORGANIZATION IS LINE SEQUENTIAL.

       DATA DIVISION.
       FILE SECTION.

       FD  EXTRACT-FILE.
       01  ODS-AI-REC.
           COPY ODSAISDREC.

       WORKING-STORAGE SECTION.

       01  WS-FILE-SYSTEM          PIC X(10) VALUE 'OMNI_SD'.
       01  SUB-UDF                 PIC S9(5) BINARY.
      
      *******************************************************************
      * OMNI Input/Output                                               *
      *******************************************************************
       01  IO-AI-PARM.
           COPY IOPARM   REPLACING 'PROGRAM' BY 'ODSAISD1'
                                     ==:XXYY:== BY ==AI==.

      *******************************************************************
      * OMNI Initialization Areas                                       *
      *******************************************************************
       01  AIV-REC.
           COPY MSTPTAIV.
 
      *******************************************************************
      * OMNI Data Areas                                                 *
      *******************************************************************
       01  BAOPENIA-AREA.
           COPY BAOPENIA REPLACING 'PROGRAM' BY 'ODSAISD1'.
       01  ENVIA-AREA.
           COPY ENVIA.
       01  RGENIA-AREA.
           COPY RGENIA.
       
       01  AI-REC.
           COPY MSTPTAI.
       01  SD-AREA.
           COPY PRMSD.


       LINKAGE SECTION.

       01  RGEN-PARM.
           COPY PRMRGEN.

       PROCEDURE DIVISION.

       1000-MAIN.

           DISPLAY 'EBSFSET' UPON ENVIRONMENT-NAME.

           CALL 'BASDIN'    USING SD-AREA.

           SET ENVIA-OP-PROCESS-INIT      TO TRUE.
           CALL 'BAENV'     USING SD-AREA
                                  ENVIA-AREA.
           SET ENVIA-OP-PARMSET-ALL       TO TRUE.
           CALL 'BAENV'     USING SD-AREA
                                  ENVIA-AREA.

           SET SD-AUTO-OPEN-YES        TO TRUE.
           SET SD-EDIT-MODE-EDIT       TO TRUE.

           SET RGENIA-OP-ALLOC-ALL TO TRUE.
           CALL 'BARGENAL'  USING SD-AREA
                                  RGENIA-AREA.

           SET ADDRESS  OF RGEN-PARM       TO SD-ADDR-RGEN-PARM.

           SET BAOPENIA-OPEN-ALL-MASTER TO TRUE.
           CALL 'BAOPEN'   USING SD-AREA
                                 BAOPENIA-AREA.
          
           OPEN OUTPUT EXTRACT-FILE.

           PERFORM 2000-READ-OMNI-DATA
           
           CLOSE EXTRACT-FILE.

           GOBACK.


       2000-READ-OMNI-DATA.

           MOVE SPACES         TO ODS-AI-REC.
           *> Read across plans - don't stop at end of a plan
           SET IO-AI-VIEW-FILE TO TRUE.
           MOVE AIV-REC        TO AI-REC.
           MOVE '000002'       TO AI-PLAN-NUM OF AI-REC.
           CALL 'PTAIIO'   USING SD-AREA
                                 IO-AI-PARM
                                 IO-AI-READ-GTEQ
                                 AI-REC.

           PERFORM 3000-PROCESS-RECORD 
             UNTIL IO-AI-EOF = 'Y'
                OR IO-AI-ERROR = 'Y'.


       3000-PROCESS-RECORD.

           PERFORM 4000-WRITE-EXTRACT.

           CALL 'PTAIIO' USING SD-AREA
                               IO-AI-PARM
                               IO-AI-READ-GT
                               AI-REC.


       4000-WRITE-EXTRACT.
  
           MOVE WS-FILE-SYSTEM       TO ODS-AI-REC.
           MOVE CORRESPONDING AI-REC TO ODS-AI-REC.
           
           INSPECT AI-DATA OF ODS-AI-REC                                 13771
                REPLACING ALL X'00' BY SPACES.                           13771
           INSPECT AI-DATA OF ODS-AI-REC                                 13771
                REPLACING ALL X'0A' BY SPACES.                           13771
           INSPECT AI-DATA OF ODS-AI-REC                                 13771
                REPLACING ALL X'0C' BY SPACES.                           13771
          
           WRITE ODS-AI-REC.
