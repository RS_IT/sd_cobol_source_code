       IDENTIFICATION DIVISION.
       PROGRAM-ID.  ODSLMSD1.

      ******************************************************************
      * Creates extract of TALM records for RS Data Strategy into RS_ODS
      *
      * #         DATE    PROG  DESCRIPTION
      * W13771  10/30/17  WCT   Original
      ******************************************************************

       ENVIRONMENT DIVISION.

       INPUT-OUTPUT SECTION.
       FILE-CONTROL.

       SELECT EXTRACT-FILE    ASSIGN TO ODSLM
                              ORGANIZATION IS LINE SEQUENTIAL.

       DATA DIVISION.
       FILE SECTION.

       FD  EXTRACT-FILE.
       01  ODS-LM-REC.
           COPY ODSLMSDREC.

       WORKING-STORAGE SECTION.

       01  WS-FILE-SYSTEM   PIC X(10) VALUE 'OMNI_SD'.

      *******************************************************************
      * OMNI Input/Output                                               *
      *******************************************************************
       01  IO-LM-PARM.
           COPY IOPARM   REPLACING 'PROGRAM' BY 'ODSLMSD1'
                                     ==:XXYY:== BY ==LM==.

      *******************************************************************
      * OMNI Initialization Areas                                       *
      *******************************************************************
       01  TALMV-REC.
           COPY MSTTALMV.
 
      *******************************************************************
      * OMNI Data Areas                                                 *
      *******************************************************************
       01  BAOPENIA-AREA.
           COPY BAOPENIA REPLACING 'PROGRAM' BY 'ODSLMSD1'.
       01  ENVIA-AREA.
           COPY ENVIA.
       01  RGENIA-AREA.
           COPY RGENIA.
       
       01  TALM-REC.
           COPY MSTTALM.
       01  SD-AREA.
           COPY PRMSD.


       LINKAGE SECTION.

       01  RGEN-PARM.
           COPY PRMRGEN.

       PROCEDURE DIVISION.

       1000-MAIN.

           DISPLAY 'EBSFSET' UPON ENVIRONMENT-NAME.
       
           CALL 'BASDIN'    USING SD-AREA.

           SET ENVIA-OP-PROCESS-INIT      TO TRUE.
           CALL 'BAENV'     USING SD-AREA
                                  ENVIA-AREA.
           SET ENVIA-OP-PARMSET-ALL       TO TRUE.
           CALL 'BAENV'     USING SD-AREA
                                  ENVIA-AREA.

           SET SD-AUTO-OPEN-YES        TO TRUE.
           SET SD-EDIT-MODE-EDIT       TO TRUE.

           SET RGENIA-OP-ALLOC-ALL TO TRUE.
           CALL 'BARGENAL'  USING SD-AREA
                                  RGENIA-AREA.

           SET ADDRESS  OF RGEN-PARM       TO SD-ADDR-RGEN-PARM.

           SET BAOPENIA-OPEN-ALL-MASTER TO TRUE.
           CALL 'BAOPEN'   USING SD-AREA
                                 BAOPENIA-AREA.
          
           OPEN OUTPUT EXTRACT-FILE.

           PERFORM 2000-READ-OMNI-DATA
           
           CLOSE EXTRACT-FILE.

           GOBACK.


       2000-READ-OMNI-DATA.

           MOVE SPACES        TO ODS-LM-REC.
           MOVE TALMV-REC     TO TALM-REC.
           CALL 'TALMIO'   USING SD-AREA
                                 IO-LM-PARM
                                 IO-LM-READ-GTEQ
                                 TALM-REC.

           PERFORM 3000-PROCESS-RECORD
             UNTIL IO-LM-EOF = 'Y'
                OR IO-LM-ERROR = 'Y'.

       3000-PROCESS-RECORD.

           PERFORM 4000-WRITE-EXTRACT.

           CALL 'TALMIO' USING SD-AREA
                               IO-LM-PARM
                               IO-LM-READ-GT
                               TALM-REC.


       4000-WRITE-EXTRACT.
  
           MOVE WS-FILE-SYSTEM TO ODS-LM-REC.
           MOVE TALM-TAX-YEAR  OF TALM-REC TO
                TALM-TAX-YEAR  OF ODS-LM-REC.
           MOVE CORRESPONDING TALM-DATA OF TALM-REC TO 
                TALM-DATA OF ODS-LM-REC.

           INSPECT TALM-DATA OF ODS-LM-REC                               13771
                REPLACING ALL X'00' BY SPACES.                           13771
           INSPECT TALM-DATA OF ODS-LM-REC                               13771
                REPLACING ALL X'0A' BY SPACES.                           13771
           INSPECT TALM-DATA OF ODS-LM-REC                               13771
                REPLACING ALL X'0C' BY SPACES.                           13771
                
           WRITE ODS-LM-REC.
