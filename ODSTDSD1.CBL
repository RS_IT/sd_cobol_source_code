       IDENTIFICATION DIVISION.
       PROGRAM-ID.  ODSTDSD1.

      ******************************************************************
      * Creates extract of VTD records for VD Data Strategy into VD_ODS
      *
      * #         DATE    PROG  DESCRIPTION
      * W13771  01/17/18  WCT   PROJ 13771 RST data integration
      ******************************************************************

       ENVIRONMENT DIVISION.

       INPUT-OUTPUT SECTION.
       FILE-CONTROL.

       SELECT EXTRACT-FILE    ASSIGN TO ODSTD
                              ORGANIZATION IS LINE SEQUENTIAL.

       DATA DIVISION.
       FILE SECTION.

       FD  EXTRACT-FILE.
       01  ODS-TD-REC.
           COPY ODSVTDSDREC.

       WORKING-STORAGE SECTION.

       01  WS-FILE-SYSTEM          PIC X(10) VALUE 'OMNI_SD'.
       01  SUB-UDF                 PIC S9(5) BINARY.
       01  hold-plan-num           pic x(06) value spaces.
       01  WK-START-DATE    PIC S9(8) COMP-3 VALUE ZEROS.
       01  WK-IO-VD-READ    PIC X(04).
       01  WS-WRITE-EXT     PIC X(01) VALUE 'N'.
           88  WRITE-EXT      VALUE 'Y'.
      *******************************************************************
      * OMNI Input/Output                                               *
      *******************************************************************
       01  IO-VD-PARM.
           COPY IOPARM   REPLACING 'PROGRAM' BY 'ODSTDSD1'
                                     ==:XXYY:== BY ==VD==.

      *******************************************************************
      * OMNI Initialization Areas                                       *
      *******************************************************************
       01  VDV-REC.
           COPY MSTRVTDV.
           
       01  DATEIAV-AREA.
           COPY DATEIAV REPLACING 'PROGRAM' BY 'ODSTDSD1'.     
 
      *******************************************************************
      * OMNI Data Areas                                                 *
      *******************************************************************
       01  BAOPENIA-AREA.
           COPY BAOPENIA REPLACING 'PROGRAM' BY 'ODSTDSD1'.
       01  ENVIA-AREA.
           COPY ENVIA.
       01  RGENIA-AREA.
           COPY RGENIA.
       01  DATEIA-AREA.
           COPY DATEIA.        
       
       01  VD-REC.
           COPY MSTRVTD.
           
       01  SD-AREA.
           COPY PRMSD.

       LINKAGE SECTION.
       01  PARM-AREA.
           05  PARM-LENGTH       PIC S9(4) BINARY SYNC.
           05  PARM-DAYS-BACK    PIC 999.        
       01  RGEN-PARM.
           COPY PRMRGEN.

       PROCEDURE DIVISION USING PARM-AREA.

       1000-MAIN.

           DISPLAY 'EBSFSET' UPON ENVIRONMENT-NAME.
          
           CALL 'BASDIN'    USING SD-AREA.

           SET ENVIA-OP-PROCESS-INIT      TO TRUE.
           CALL 'BAENV'     USING SD-AREA
                                  ENVIA-AREA.
           SET ENVIA-OP-PARMSET-ALL       TO TRUE.
           CALL 'BAENV'     USING SD-AREA
                                  ENVIA-AREA.

           SET SD-AUTO-OPEN-YES        TO TRUE.
           SET SD-EDIT-MODE-EDIT       TO TRUE.

           SET RGENIA-OP-ALLOC-ALL TO TRUE.
           CALL 'BARGENAL'  USING SD-AREA
                                  RGENIA-AREA.

           SET ADDRESS  OF RGEN-PARM       TO SD-ADDR-RGEN-PARM.

           SET BAOPENIA-OPEN-ALL-MASTER TO TRUE.
           CALL 'BAOPEN'   USING SD-AREA
                                 BAOPENIA-AREA.
          
           OPEN OUTPUT EXTRACT-FILE.
           
           *> When set, will limit to TD records pulled to the
           *> number of days back from the Run Date.
           *> When not set, will pull all TD records.
           IF PARM-DAYS-BACK > 0
              MOVE DATEIAV-AREA           TO DATEIA-AREA
              MOVE PARM-DAYS-BACK         TO DATEIA-NUM
              SET DATEIA-RC-CLEAR         TO TRUE
              SET DATEIA-FREQ-DAILY       TO TRUE
              SET DATEIA-OP-CALC-BACKWARD TO TRUE
              MOVE SD-RUN-DATE            TO DATEIA-DATE
              CALL 'UTDTPROC' USING SD-AREA
                                    DATEIA-AREA
              MOVE DATEIA-RETURN-YMD      TO WK-START-DATE
           END-IF.                                            
 
           PERFORM 2000-READ-OMNI-DATA.
           
           CLOSE EXTRACT-FILE.

           GOBACK.

       2000-READ-OMNI-DATA.
           SET IO-VD-VIEW-FILE TO TRUE.
           *> Read across plans - don't stop at end of a plan
           MOVE SPACES         TO ODS-TD-REC.
           MOVE VDV-REC        TO VD-REC
           MOVE '000002'       TO VTD-PLAN-NUM OF VD-REC.
           CALL 'VTTDIO'   USING SD-AREA
                                 IO-VD-PARM
                                 IO-VD-READ-GTEQ
                                 VD-REC.

           PERFORM 3000-PROCESS-RECORD 
             UNTIL IO-VD-EOF   = 'Y'  
                OR IO-VD-ERROR = 'Y'.

       3000-PROCESS-RECORD.
                  
           MOVE 'N' TO WS-WRITE-EXT.
       
           IF ((VTD-CREATE-DATE OF VD-REC >= WK-START-DATE)
              OR (VTD-UPDATE-DATE OF VD-REC >= WK-START-DATE)
                OR (VTD-POST-DATE OF VD-REC >= WK-START-DATE))
                 MOVE 'Y' TO WS-WRITE-EXT
           END-IF.
           
           IF WRITE-EXT      
              PERFORM 4000-WRITE-EXTRACT
           END-IF.
           
           MOVE IO-VD-READ-GT   TO WK-IO-VD-READ.         
         
           CALL 'VTTDIO' USING SD-AREA
                               IO-VD-PARM
                               WK-IO-VD-READ
                               VD-REC.

       4000-WRITE-EXTRACT.
           MOVE WS-FILE-SYSTEM       TO ODS-TD-REC.
           MOVE CORRESPONDING VD-REC TO ODS-TD-REC. 
           MOVE VTD-ENTRY(1)        TO VTD-CARD-DATA-1.  
           MOVE VTD-ENTRY(2)        TO VTD-CARD-DATA-2.
           MOVE VTD-ENTRY(3)        TO VTD-CARD-DATA-3.
           MOVE VTD-ENTRY(4)        TO VTD-CARD-DATA-4.
           MOVE VTD-ENTRY(5)        TO VTD-CARD-DATA-5. 
           MOVE VTD-ENTRY(6)        TO VTD-CARD-DATA-6.
           MOVE VTD-ENTRY(7)        TO VTD-CARD-DATA-7.      
           MOVE VTD-ENTRY(8)        TO VTD-CARD-DATA-8.  
           MOVE VTD-ENTRY(9)        TO VTD-CARD-DATA-9.
           MOVE VTD-ENTRY(10)       TO VTD-CARD-DATA-10.
           MOVE VTD-ENTRY(11)       TO VTD-CARD-DATA-11.      
           MOVE VTD-ENTRY(12)       TO VTD-CARD-DATA-12.    
           MOVE VTD-ENTRY(13)       TO VTD-CARD-DATA-13.    
           MOVE VTD-ENTRY(14)       TO VTD-CARD-DATA-14.    
           MOVE VTD-ENTRY(15)       TO VTD-CARD-DATA-15.    
           MOVE VTD-ENTRY(16)       TO VTD-CARD-DATA-16.    
           MOVE VTD-ENTRY(17)       TO VTD-CARD-DATA-17.    
           MOVE VTD-ENTRY(18)       TO VTD-CARD-DATA-18.    
           MOVE VTD-ENTRY(19)       TO VTD-CARD-DATA-19.    
           MOVE VTD-ENTRY(20)       TO VTD-CARD-DATA-20.
           MOVE VTD-ENTRY(21)       TO VTD-CARD-DATA-21.       
           MOVE VTD-ENTRY(22)       TO VTD-CARD-DATA-22.    
           MOVE VTD-ENTRY(23)       TO VTD-CARD-DATA-23.    
           MOVE VTD-ENTRY(24)       TO VTD-CARD-DATA-24.    
           MOVE VTD-ENTRY(25)       TO VTD-CARD-DATA-25.    
           MOVE VTD-ENTRY(26)       TO VTD-CARD-DATA-26.    
           MOVE VTD-ENTRY(27)       TO VTD-CARD-DATA-27.    
           MOVE VTD-ENTRY(28)       TO VTD-CARD-DATA-28.    
           MOVE VTD-ENTRY(29)       TO VTD-CARD-DATA-29.    
           MOVE VTD-ENTRY(30)       TO VTD-CARD-DATA-30.  
           MOVE VTD-ENTRY(31)       TO VTD-CARD-DATA-31.     
           MOVE VTD-ENTRY(32)       TO VTD-CARD-DATA-32.     
           MOVE VTD-ENTRY(33)       TO VTD-CARD-DATA-33.     
           MOVE VTD-ENTRY(34)       TO VTD-CARD-DATA-34.     
           MOVE VTD-ENTRY(35)       TO VTD-CARD-DATA-35.     
           MOVE VTD-ENTRY(36)       TO VTD-CARD-DATA-36.     
           MOVE VTD-ENTRY(37)       TO VTD-CARD-DATA-37.     
           MOVE VTD-ENTRY(38)       TO VTD-CARD-DATA-38.     
           MOVE VTD-ENTRY(39)       TO VTD-CARD-DATA-39.     
           MOVE VTD-ENTRY(40)       TO VTD-CARD-DATA-40.
           MOVE VTD-ENTRY(41)       TO VTD-CARD-DATA-41.         
           MOVE VTD-ENTRY(42)       TO VTD-CARD-DATA-42.     
           MOVE VTD-ENTRY(43)       TO VTD-CARD-DATA-43.     
           MOVE VTD-ENTRY(44)       TO VTD-CARD-DATA-44.     
           MOVE VTD-ENTRY(45)       TO VTD-CARD-DATA-45.     
           MOVE VTD-ENTRY(46)       TO VTD-CARD-DATA-46.     
           MOVE VTD-ENTRY(47)       TO VTD-CARD-DATA-47.     
           MOVE VTD-ENTRY(48)       TO VTD-CARD-DATA-48.     
           MOVE VTD-ENTRY(49)       TO VTD-CARD-DATA-49.     
           MOVE VTD-ENTRY(50)       TO VTD-CARD-DATA-50. 
           MOVE VTD-ENTRY(51)       TO VTD-CARD-DATA-51.        
           MOVE VTD-ENTRY(52)       TO VTD-CARD-DATA-52.     
           MOVE VTD-ENTRY(53)       TO VTD-CARD-DATA-53.     
           MOVE VTD-ENTRY(54)       TO VTD-CARD-DATA-54.     
           MOVE VTD-ENTRY(55)       TO VTD-CARD-DATA-55.     
           MOVE VTD-ENTRY(56)       TO VTD-CARD-DATA-56.     
           MOVE VTD-ENTRY(57)       TO VTD-CARD-DATA-57.     
           MOVE VTD-ENTRY(58)       TO VTD-CARD-DATA-58.     
           MOVE VTD-ENTRY(59)       TO VTD-CARD-DATA-59.     
           MOVE VTD-ENTRY(60)       TO VTD-CARD-DATA-60.
           MOVE VTD-ENTRY(61)       TO VTD-CARD-DATA-61.         
           MOVE VTD-ENTRY(62)       TO VTD-CARD-DATA-62.     
           MOVE VTD-ENTRY(63)       TO VTD-CARD-DATA-63.     
           MOVE VTD-ENTRY(64)       TO VTD-CARD-DATA-64.     
           MOVE VTD-ENTRY(65)       TO VTD-CARD-DATA-65.     
           MOVE VTD-ENTRY(66)       TO VTD-CARD-DATA-66.      
           MOVE VTD-ENTRY(67)       TO VTD-CARD-DATA-67.     
           MOVE VTD-ENTRY(68)       TO VTD-CARD-DATA-68.     
           MOVE VTD-ENTRY(69)       TO VTD-CARD-DATA-69.     
           MOVE VTD-ENTRY(70)       TO VTD-CARD-DATA-70. 
           MOVE VTD-ENTRY(71)       TO VTD-CARD-DATA-71.       
           MOVE VTD-ENTRY(72)       TO VTD-CARD-DATA-72.    
           MOVE VTD-ENTRY(73)       TO VTD-CARD-DATA-73.    
           MOVE VTD-ENTRY(74)       TO VTD-CARD-DATA-74.    
           MOVE VTD-ENTRY(75)       TO VTD-CARD-DATA-75.    
           MOVE VTD-ENTRY(76)       TO VTD-CARD-DATA-76.    
           MOVE VTD-ENTRY(77)       TO VTD-CARD-DATA-77.    
           MOVE VTD-ENTRY(78)       TO VTD-CARD-DATA-78.    
           MOVE VTD-ENTRY(79)       TO VTD-CARD-DATA-79.    
           MOVE VTD-ENTRY(80)       TO VTD-CARD-DATA-80. 
           MOVE VTD-ENTRY(81)       TO VTD-CARD-DATA-81.       
           MOVE VTD-ENTRY(82)       TO VTD-CARD-DATA-82.     
           MOVE VTD-ENTRY(83)       TO VTD-CARD-DATA-83.     
           MOVE VTD-ENTRY(84)       TO VTD-CARD-DATA-84.     
           MOVE VTD-ENTRY(85)       TO VTD-CARD-DATA-85.     
           MOVE VTD-ENTRY(86)       TO VTD-CARD-DATA-86.     
           MOVE VTD-ENTRY(87)       TO VTD-CARD-DATA-87.     
           MOVE VTD-ENTRY(88)       TO VTD-CARD-DATA-88.     
           MOVE VTD-ENTRY(89)       TO VTD-CARD-DATA-89.     
           MOVE VTD-ENTRY(90)       TO VTD-CARD-DATA-90.  
           MOVE VTD-ENTRY(91)       TO VTD-CARD-DATA-91.    
           MOVE VTD-ENTRY(92)       TO VTD-CARD-DATA-92.    
           MOVE VTD-ENTRY(93)       TO VTD-CARD-DATA-93.    
           MOVE VTD-ENTRY(94)       TO VTD-CARD-DATA-94.    
           MOVE VTD-ENTRY(95)       TO VTD-CARD-DATA-95.    
           MOVE VTD-ENTRY(96)       TO VTD-CARD-DATA-96.    
           MOVE VTD-ENTRY(97)       TO VTD-CARD-DATA-97.    
           MOVE VTD-ENTRY(98)       TO VTD-CARD-DATA-98.      
           MOVE VTD-ENTRY(99)       TO VTD-CARD-DATA-99.    
           MOVE VTD-ENTRY(100)      TO VTD-CARD-DATA-100.
           MOVE VTD-ENTRY(101)      TO VTD-CARD-DATA-101.       
           MOVE VTD-ENTRY(102)      TO VTD-CARD-DATA-102.    
           MOVE VTD-ENTRY(103)      TO VTD-CARD-DATA-103.    
           MOVE VTD-ENTRY(104)      TO VTD-CARD-DATA-104.    
           MOVE VTD-ENTRY(105)      TO VTD-CARD-DATA-105.    
           MOVE VTD-ENTRY(106)      TO VTD-CARD-DATA-106.    
           MOVE VTD-ENTRY(107)      TO VTD-CARD-DATA-107.    
           MOVE VTD-ENTRY(108)      TO VTD-CARD-DATA-108.    
           MOVE VTD-ENTRY(109)      TO VTD-CARD-DATA-109.    
           MOVE VTD-ENTRY(110)      TO VTD-CARD-DATA-110.  
           MOVE VTD-ENTRY(111)      TO VTD-CARD-DATA-111.  
           MOVE VTD-ENTRY(112)      TO VTD-CARD-DATA-112. 
           MOVE VTD-ENTRY(113)      TO VTD-CARD-DATA-113. 
           MOVE VTD-ENTRY(114)      TO VTD-CARD-DATA-114.   
           MOVE VTD-ENTRY(115)      TO VTD-CARD-DATA-115. 
           MOVE VTD-ENTRY(116)      TO VTD-CARD-DATA-116. 
           MOVE VTD-ENTRY(117)      TO VTD-CARD-DATA-117. 
           MOVE VTD-ENTRY(118)      TO VTD-CARD-DATA-118. 
           
           INSPECT VTD-DATA OF ODS-TD-REC                               13771 
                REPLACING ALL X'00' BY SPACES.                          13771
           INSPECT VTD-DATA OF ODS-TD-REC                               13771
                REPLACING ALL X'0A' BY SPACES.                          13771
           INSPECT VTD-DATA OF ODS-TD-REC                               13771
                REPLACING ALL X'0C' BY SPACES.                          13771
           WRITE ODS-TD-REC.
