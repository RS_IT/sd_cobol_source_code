       IDENTIFICATION DIVISION.
       PROGRAM-ID.  ODSCMSD1.

      ******************************************************************
      * Creates extract of CM records for RS Data Strategy into CM_ODS
      *
      * #         DATE    PROG  DESCRIPTION
      * W13771  11/29/17  WCT   PROJ 13771 RST data integration
      ******************************************************************

       ENVIRONMENT DIVISION.

       INPUT-OUTPUT SECTION.
       FILE-CONTROL.

       SELECT EXTRACT-FILE    ASSIGN TO ODSCM
                              ORGANIZATION IS LINE SEQUENTIAL.

       DATA DIVISION.
       FILE SECTION.

       FD  EXTRACT-FILE.
       01  ODS-CM-REC.
           COPY ODSCMSDREC.

       WORKING-STORAGE SECTION.

       01  WS-FILE-SYSTEM          PIC X(10) VALUE 'OMNI_SD'.
       01  SUB-UDF                 PIC S9(5) BINARY.
      
      *******************************************************************
      * OMNI Input/Output                                               *
      *******************************************************************
       01  IO-CM-PARM.
           COPY IOPARM   REPLACING 'PROGRAM' BY 'ODSCMSD1'
                                     ==:XXYY:== BY ==CM==.

      *******************************************************************
      * OMNI Initialization Areas                                       *
      *******************************************************************
       01  CMV-REC.
           COPY MSTCMCMV.
 
      *******************************************************************
      * OMNI Data Areas                                                 *
      *******************************************************************
       01  BAOPENIA-AREA.
           COPY BAOPENIA REPLACING 'PROGRAM' BY 'ODSCMSD1'.
       01  ENVIA-AREA.
           COPY ENVIA.
       01  RGENIA-AREA.
           COPY RGENIA.
       
       01  CM-REC.
           COPY MSTCMCM.
       01  SD-AREA.
           COPY PRMSD.


       LINKAGE SECTION.

       01  RGEN-PARM.
           COPY PRMRGEN.

       PROCEDURE DIVISION.

       1000-MAIN.

           DISPLAY 'EBSFSET' UPON ENVIRONMENT-NAME.

           CALL 'BASDIN'    USING SD-AREA.

           SET ENVIA-OP-PROCESS-INIT      TO TRUE.
           CALL 'BAENV'     USING SD-AREA
                                  ENVIA-AREA.
           SET ENVIA-OP-PARMSET-ALL       TO TRUE.
           CALL 'BAENV'     USING SD-AREA
                                  ENVIA-AREA.

           SET SD-AUTO-OPEN-YES        TO TRUE.
           SET SD-EDIT-MODE-EDIT       TO TRUE.

           SET RGENIA-OP-ALLOC-ALL TO TRUE.
           CALL 'BARGENAL'  USING SD-AREA
                                  RGENIA-AREA.

           SET ADDRESS  OF RGEN-PARM       TO SD-ADDR-RGEN-PARM.

           SET BAOPENIA-OPEN-ALL-MASTER TO TRUE.
           CALL 'BAOPEN'   USING SD-AREA
                                 BAOPENIA-AREA.
          
           OPEN OUTPUT EXTRACT-FILE.

           PERFORM 2000-READ-OMNI-DATA
           
           CLOSE EXTRACT-FILE.

           GOBACK.


       2000-READ-OMNI-DATA.

           MOVE SPACES         TO ODS-CM-REC.
           *> Read across plans - don't stop at end of a plan
           SET IO-CM-VIEW-FILE TO TRUE.
           MOVE CMV-REC        TO CM-REC.
           MOVE '000002'       TO CM-PLAN-NUM OF CM-REC.
           CALL 'CMCMIO'   USING SD-AREA
                                 IO-CM-PARM
                                 IO-CM-READ-GTEQ
                                 CM-REC.

           PERFORM 3000-PROCESS-RECORD 
             UNTIL IO-CM-EOF = 'Y'
                OR IO-CM-ERROR = 'Y'.


       3000-PROCESS-RECORD.

           PERFORM 4000-WRITE-EXTRACT.

           CALL 'CMCMIO' USING SD-AREA
                               IO-CM-PARM
                               IO-CM-READ-GT
                               CM-REC.


       4000-WRITE-EXTRACT.
  
           MOVE WS-FILE-SYSTEM       TO ODS-CM-REC.
           MOVE CORRESPONDING CM-REC TO ODS-CM-REC.
           INSPECT CM-DATA OF ODS-CM-REC                                 13771
                REPLACING ALL X'00' BY SPACES.                           13771
           INSPECT CM-DATA OF ODS-CM-REC                                 13771
                REPLACING ALL X'0A' BY SPACES.                           13771
           INSPECT CM-DATA OF ODS-CM-REC                                 13771
                REPLACING ALL X'0C' BY SPACES.                           13771
           
           WRITE ODS-CM-REC.

