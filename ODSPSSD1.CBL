       IDENTIFICATION DIVISION.
       PROGRAM-ID.  ODSPSSD1.

      ******************************************************************
      * Creates extract of PS records for RS Data Strategy into RS_ODS
      *
      * #         DATE    PROG  DESCRIPTION
      * W13771   01/08/18  WCT  Original
      ******************************************************************

       ENVIRONMENT DIVISION.

       INPUT-OUTPUT SECTION.
       FILE-CONTROL.

       SELECT EXTRACT-FILE    ASSIGN TO ODSPS
                              ORGANIZATION IS LINE SEQUENTIAL.

       DATA DIVISION.
       FILE SECTION.

       FD  EXTRACT-FILE.
       01  ODS-PS-REC.
           COPY ODSPSSDREC.

       WORKING-STORAGE SECTION.

       01  WS-FILE-SYSTEM  PIC X(10) VALUE 'OMNI_SD'.

      *******************************************************************
      * OMNI Input/Output                                               *
      *******************************************************************
       01  IO-PS-PARM.
           COPY IOPARM   REPLACING 'PROGRAM' BY 'ODSPSSD1'
                                     ==:XXYY:== BY ==PS==.

      *******************************************************************
      * OMNI Initialization Areas                                       *
      *******************************************************************
       01  PSV-REC.
           COPY MSTPTPSV.
 
      *******************************************************************
      * OMNI Data Areas                                                 *
      *******************************************************************
       01  BAOPENIA-AREA.
           COPY BAOPENIA REPLACING 'PROGRAM' BY 'ODSPSSD1'.
       01  ENVIA-AREA.
           COPY ENVIA.
       01  RGENIA-AREA.
           COPY RGENIA.
       
       01  PS-REC.
           COPY MSTPTPS.
       01  SD-AREA.
           COPY PRMSD.


       LINKAGE SECTION.

       01  RGEN-PARM.
           COPY PRMRGEN.

       PROCEDURE DIVISION.

       1000-MAIN.

           DISPLAY 'EBSFSET' UPON ENVIRONMENT-NAME.

           CALL 'BASDIN'    USING SD-AREA.

           SET ENVIA-OP-PROCESS-INIT      TO TRUE.
           CALL 'BAENV'     USING SD-AREA
                                  ENVIA-AREA.
           SET ENVIA-OP-PARMSET-ALL       TO TRUE.
           CALL 'BAENV'     USING SD-AREA
                                  ENVIA-AREA.

           SET SD-AUTO-OPEN-YES        TO TRUE.
           SET SD-EDIT-MODE-EDIT       TO TRUE.

           SET RGENIA-OP-ALLOC-ALL TO TRUE.
           CALL 'BARGENAL'  USING SD-AREA
                                  RGENIA-AREA.

           SET ADDRESS  OF RGEN-PARM       TO SD-ADDR-RGEN-PARM.

           SET BAOPENIA-OPEN-ALL-MASTER TO TRUE.
           CALL 'BAOPEN'   USING SD-AREA
                                 BAOPENIA-AREA.
          
           OPEN OUTPUT EXTRACT-FILE.

           PERFORM 2000-READ-OMNI-DATA
           
           CLOSE EXTRACT-FILE.

           GOBACK.


       2000-READ-OMNI-DATA.

           MOVE SPACES         TO ODS-PS-REC.
           *> Read across plans - don't stop at end of a plan
           SET IO-PS-VIEW-FILE TO TRUE.
           MOVE PSV-REC        TO PS-REC.
           MOVE '000002'       TO PS-PLAN-NUM OF PS-REC.
           CALL 'PTPSIO'   USING SD-AREA
                                 IO-PS-PARM
                                 IO-PS-READ-GTEQ
                                 PS-REC.

           PERFORM 3000-PROCESS-RECORD 
             UNTIL IO-PS-EOF = 'Y'
                OR IO-PS-ERROR = 'Y'.


       3000-PROCESS-RECORD.

           IF PS-SRC OF PS-REC > SPACES
              PERFORM 4000-WRITE-EXTRACT
           END-IF.

           CALL 'PTPSIO' USING SD-AREA 
                               IO-PS-PARM
                               IO-PS-READ-GT
                               PS-REC.


       4000-WRITE-EXTRACT.
  
           MOVE WS-FILE-SYSTEM       TO ODS-PS-REC.
           MOVE CORRESPONDING PS-REC TO ODS-PS-REC.
   
           MOVE PS-UDF-DATE(1)   TO PS-UDF-DATE-1. 
           MOVE PS-UDF-DATE(2)   TO PS-UDF-DATE-2.
           MOVE PS-UDF-DATE(3)   TO PS-UDF-DATE-3.
           MOVE PS-UDF-DATE(4)   TO PS-UDF-DATE-4.
           MOVE PS-UDF-RATE(1)   TO PS-UDF-RATE-1.
           MOVE PS-UDF-RATE(2)   TO PS-UDF-RATE-2.
           MOVE PS-UDF-RATE(3)   TO PS-UDF-RATE-3.
           MOVE PS-UDF-RATE(4)   TO PS-UDF-RATE-4.
           MOVE PS-UDF-RATE(5)   TO PS-UDF-RATE-5.
           MOVE PS-UDF-RATE(6)   TO PS-UDF-RATE-6.
           MOVE PS-UDF-DOLLAR(1) TO PS-UDF-DOLLAR-1.
           MOVE PS-UDF-DOLLAR(2) TO PS-UDF-DOLLAR-2.
           MOVE PS-UDF-DOLLAR(3) TO PS-UDF-DOLLAR-3.
           MOVE PS-UDF-DOLLAR(4) TO PS-UDF-DOLLAR-4.
           MOVE PS-UDF-DOLLAR(5) TO PS-UDF-DOLLAR-5.
           MOVE PS-UDF-DOLLAR(6) TO PS-UDF-DOLLAR-6.
   
           INSPECT PS-DATA OF ODS-PS-REC
                REPLACING ALL X'00' BY SPACES.
           INSPECT PS-DATA OF ODS-PS-REC
                REPLACING ALL X'0A' BY SPACES.
           INSPECT PS-DATA OF ODS-PS-REC
                REPLACING ALL X'0C' BY SPACES.

           WRITE ODS-PS-REC.
