       IDENTIFICATION DIVISION.
       PROGRAM-ID.  ODSNHSD1.

      ******************************************************************
      * Creates extract of EQ record for RS Data Strategy
      * into RS_ODS
      *
      * #         DATE    PROG  DESCRIPTION
      * W13771  11/28/17  KLM   PROJ13771 RST data integration
      ******************************************************************

       ENVIRONMENT DIVISION.

       INPUT-OUTPUT SECTION.
       FILE-CONTROL.

       SELECT EXTRACT-NH-FILE ASSIGN TO ODSNH
                              ORGANIZATION IS LINE SEQUENTIAL.

       DATA DIVISION.
       FILE SECTION.

       FD  EXTRACT-NH-FILE.
       01  ODS-NH-REC.
           COPY ODSNHSDREC.

       WORKING-STORAGE SECTION.

       01  WS-FILE-SYSTEM          PIC X(10) VALUE SPACES.
       01  SUB-UDF                 PIC S9(5) BINARY.
      
      *******************************************************************
      * OMNI Input/Output                                               *
      *******************************************************************
       01 IO-NTNH-PARM.                                                
          COPY IOPARM REPLACING 'PROGRAM' BY 'ODSNHSD1'                
                                ==:XXYY:== BY ==NTNH==.               

      *******************************************************************
      * OMNI Initialization Areas                                       *
      *******************************************************************
       01  NHV-REC.
           COPY MSTNTNHV.    
       01  DEV-AREA.
           COPY PRMDE2.
 
      *******************************************************************
      * OMNI Data Areas                                                 *
      *******************************************************************
       01  BAOPENIA-AREA.
           COPY BAOPENIA REPLACING 'PROGRAM' BY 'ODSNHSD1'.
       01  ENVIA-AREA.
           COPY ENVIA.
       01  RGENIA-AREA.
           COPY RGENIA.
       
       01  DE-AREA.
           COPY PRMDE.
       01  SD-AREA.
           COPY PRMSD.


       LINKAGE SECTION.

       01  RGEN-PARM.
           COPY PRMRGEN.
       01  AH-REC.                                                       
           COPY AHDEF.                            
         
       01  NH-REC REDEFINES AH-REC.                                    
           COPY MSTNTNH.                                                
 
       PROCEDURE DIVISION.

       1000-MAIN.

           DISPLAY 'EBSFSET' UPON ENVIRONMENT-NAME.
           MOVE 'OMNI_SD' TO WS-FILE-SYSTEM.
           CALL 'BASDIN'    USING SD-AREA.

           SET ENVIA-OP-PROCESS-INIT    TO TRUE.
           CALL 'BAENV'     USING SD-AREA
                                  ENVIA-AREA.
           SET ENVIA-OP-PARMSET-ALL     TO TRUE.
           CALL 'BAENV'     USING SD-AREA
                                  ENVIA-AREA.

           SET SD-AUTO-OPEN-YES         TO TRUE.
           SET SD-EDIT-MODE-EDIT        TO TRUE.          
           SET RGENIA-OP-ALLOC-ALL      TO TRUE.
           CALL 'BARGENAL'  USING SD-AREA
                                  RGENIA-AREA.

           SET ADDRESS  OF RGEN-PARM    TO SD-ADDR-RGEN-PARM.
           SET ADDRESS  OF AH-REC       TO SD-ADDR-AH.

           SET BAOPENIA-OPEN-ALL-MASTER TO TRUE.
           CALL 'BAOPEN'   USING SD-AREA
                                 BAOPENIA-AREA.

           OPEN OUTPUT EXTRACT-NH-FILE.

           PERFORM 2000-READ-OMNI-DATA
           
           CLOSE EXTRACT-NH-FILE.

           GOBACK.


       2000-READ-OMNI-DATA.

           MOVE SPACES        TO ODS-NH-REC.
           MOVE NHV-REC       TO NH-REC.
           MOVE '000002'      TO NH-PLAN-NUM OF NH-REC.
          
    
           CALL 'NTNHIO' USING SD-AREA                           
                         IO-NTNH-PARM                            
                         IO-NTNH-READ-GTEQ                       
                         AH-REC.                                 
      
          
           PERFORM 3000-PROCESS-RECORD 
             UNTIL IO-NTNH-EOF = 'Y'
                OR IO-NTNH-ERROR = 'Y'.


       3000-PROCESS-RECORD.

           PERFORM 4000-WRITE-EXTRACT.

           CALL 'NTNHIO' USING SD-AREA                           
                         IO-NTNH-PARM                            
                         IO-NTNH-READ-GT                         
                         AH-REC.           


       4000-WRITE-EXTRACT.
  
           MOVE WS-FILE-SYSTEM       TO ODS-NH-REC.
           MOVE CORRESPONDING NH-REC TO ODS-NH-REC.
           INSPECT NH-DATA OF ODS-NH-REC                                 13771
                REPLACING ALL X'00' BY SPACES.                           13771
           INSPECT NH-DATA OF ODS-NH-REC                                 13771
                REPLACING ALL X'0A' BY SPACES.                           13771
           INSPECT NH-DATA OF ODS-NH-REC                                 13771
                REPLACING ALL X'0C' BY SPACES.                           13771
           WRITE ODS-NH-REC.
